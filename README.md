A full service grocery store offering chef prepared foods, quality meats & seafood, conventional & organic produce & grocery items. We also have an in store full service pharmacy & a large selection of beer, wine & liquor.

Address: 100 Verde Valley School Road, Sedona, AZ 86351, USA

Phone: 928-284-1144

Website: https://www.clarksmarket.com
